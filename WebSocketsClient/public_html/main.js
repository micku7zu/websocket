/*
    Parallel Ping Pong
    Author: Șandor Sergiu

    Credits: http://threejs.org/
             http://jquery.com/
             http://www.threejsgames.com/extensions/
             https://code.google.com/p/dat-gui/
             https://github.com/sole/tween.js/
*/



function Person(id, name, posX, posY, color, score) {
    this.id = id;
    this.name = name;
    this.posX = posX;
    this.posY = posY;
    this.color = color;
    this.target = {"x":"-1", "y":"-1"},
    this.targeted = false,
    this.score = score,
    
    this.setTarget = function(x, y) {
        
        if(this.target["x"] != "-1")
            GUI.setFloorColor(this.target["x"], this.target["y"])
        
        this.target = {"x": x, "y": y};
        this.targeted = true;
        
        GUI.setFloorColorHex(x, y, this.color);
    },
            
    this.setTargetNull = function(x, y) {
        
        if(this.target["x"] != "-1")
            GUI.setFloorColor(this.target["x"], this.target["y"])
        
        this.target = {"x":"-1", "y":"-1"};
        this.targeted = false;
        
        GUI.setFloorColor(x, y);
    }
    
}

var CLIENT = {
    clientsDiv: $(".people"),
    chatDiv: $(".chat"),
    clients: {},
    socket: "",
    name: "",
    id: "",
    
    updateCounter: function() {
        var targeted = 0;
        for(var id in CLIENT.clients) {
            if (CLIENT.clients[id].targeted == true){
                targeted++;
            }
        }
        size = getSize(CLIENT.clients);
        $(".counter").html(size + " online <br />Targeted: " + targeted + "/" + size);
    },
    
    addNew: function(id, name, posX, posY, color, score) {
        CLIENT.clients[id] = new Person(id, name, posX, posY, color, score);
        var div = $('<div class="person" style="display:none;" id="person-' + id +'"><div class="background" style="background-color:'+color+';"></div><div class="name">' + name + '(' + score + ')</div></div>');
        CLIENT.clientsDiv.append(div);
        div.slideDown("slow");
        CLIENT.updateCounter();
        
        GUI.createTennis(id);
    },
    
    remove: function(id) {
        CLIENT.clients[id].setTargetNull();
        delete CLIENT.clients[id];
        
        $("#person-" + id).slideUp("slow", function() {
            $(this).remove();
        });
        CLIENT.updateCounter();
        GUI.deleteTennis(id);
    },
    
    createSocket: function(host, port) {
        name = "";
        while(name === "") {
            name = prompt("Enter your name:");
            name = name.replace(/\W/g, '').replace(/\s+/g, '').substring(0, 10);
        }
        CLIENT.name = name;
        CLIENT.socket = new WebSocket("ws://" + host + ":" + port, ['game']);
        CLIENT.socket.onmessage = this.onMessage;
        CLIENT.socket.onopen = this.onConnected;
        
        CLIENT.initHtml();
    },
    
    initHtml: function() {
        $("#chatForm").on("submit", function(e){
            var msg = $("#message").val();
            if(msg != "") {
                msg = msg.substring(0, 255);
                CLIENT.sendMessage({"action":"message", "message": msg});
            }
            $("#message").val("");
            
            e.preventDefault();
            return false;
        });
    },
    onMessage: function(message) {
        data = JSON.parse(message.data);
        
        console.log(data);

        switch(data["action"]) {
            
            case 'init':
                GUI.neo.posX = data.neoX;
                GUI.neo.posY = data.neoY;
                
                GUI.maxFloorX = data.maxX;
                GUI.maxFloorY = data.maxY;
                GUI.personDist = data.personDist;
                CLIENT.id = data.id;
                
                
                GUI.init();
                GUI.animate();
                break;
            
            case 'neo':
                
                clearTimeout(GUI.timeoutRestart);
                
                GUI.arrangeBalls();
                GUI.neo.setPosition(data.posX, data.posY, true);
                
                GUI.shoot();
                
                GUI.timeoutRestart = setTimeout(function(){
                    console.log("INAPOI!");
                    GUI.arrangeBalls();
                }, 3000);
                
                for(person in CLIENT.clients){
                    CLIENT.clients[person].setTargetNull();
                }
                
                CLIENT.updateCounter();
                
                break;
            case 'new':
                CLIENT.addNew(data.id, data.name, data.posX, data.posY, data.color, data.score);
                break;
               
            case 'score':
                CLIENT.clients[data.id].score = data.score;
                $("#person-" + data.id +" .name").html(CLIENT.clients[data.id].name +"("+data.score+")");
                CLIENT.appendMessage(data.id, "HIT IT!");
                break;
                
            case 'target':
                if(data.posX == "-1") {
                    CLIENT.clients[data.id].setTargetNull();
                } else {
                    CLIENT.clients[data.id].setTarget(data.posX, data.posY);
                    //CLIENT.appendMessage(data.id, "Targeted: " + data.posX + ", " + data.posY);
                }
                
                
                CLIENT.updateCounter();
                break;
                

            case 'remove':
                CLIENT.remove(data.id);
                break;
                
            case 'score':
                
                break;
                
            case 'message':
                CLIENT.appendMessage(data.id, data.message);
                break;
            default:
                break;
        }
    },
    
    appendMessage: function(id, message) {
        var msg = $('<div class="message" style="display: none;"><div class="user" style="background-color:'+CLIENT.clients[id].color+';">'+CLIENT.clients[id].name+'</div><div class="text">: '+message+'</div></div>');
        CLIENT.chatDiv.prepend(msg);
        msg.slideDown("slow");
    },
    
    onConnected: function(event) {
        console.log("Connected");
        CLIENT.sendMessage({"action":"name", "name":CLIENT.name});
        
    },
    
    setTarget: function(x, y) {
        CLIENT.sendMessage({"action":"target", "posX": x, "posY": y});
    },
    
    sendMessage: function(text) {
        if(typeof(text) === "object") {
            text = JSON.stringify(text);
        }
        CLIENT.socket.send(text);
    }
};


var GUI = GUI || {
    step: 50,
    keyboard: new THREEx.KeyboardState(),
    clock: new THREE.Clock(),
    neo: {},
    mouse: {
        x: 0,
        y: 0
    },
    onRenderFunctions: [],
    balls: {}
};

GUI.notTargeted = function(x, y) {
    for(client in CLIENT.clients){
        client = CLIENT.clients[client];
        if (client.target["x"] == x && client.target["y"] == y){
            return false;
        }
    }
    return true;
}

GUI.arrangeBalls = function() {
    for(var i in CLIENT.clients) {
        person = CLIENT.clients[i];
        ball = GUI.balls[i];
        ball.position.set(person.posX * GUI.step, -50, person.posY * GUI.step);
    }
}

GUI.createLiveStats = function (container) {
    GUI.stats = new Stats();
    GUI.stats.domElement.style.position = 'absolute';
    GUI.stats.domElement.style.bottom = '0px';
    GUI.stats.domElement.style.zIndex = 100;
    container.appendChild(GUI.stats.domElement);
};


GUI.init = function () {

    THREEx.SportBalls.baseURL = "";

    GUI.scene = new THREE.Scene();

    var SCREEN_WIDTH = window.innerWidth,
        SCREEN_HEIGHT = window.innerHeight;

    GUI.camera = new THREE.PerspectiveCamera(45, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 20000);
    GUI.scene.add(GUI.camera);
    GUI.camera.position.set(GUI.step * GUI.maxFloorX / 2, 300, GUI.step * (GUI.maxFloorY + 10));
    

    if (Detector.webgl) {
        GUI.renderer = new THREE.WebGLRenderer({
            alpha: true,
            antialias: true
        });
    } else {
        GUI.renderer = new THREE.CanvasRenderer();
    }

    GUI.renderer.setClearColor(0x555555, 1);
    GUI.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);

    GUI.container = document.getElementById('ThreeJS');
    GUI.container.appendChild(GUI.renderer.domElement);

    GUI.createLiveStats(GUI.container);

    THREEx.WindowResize(GUI.renderer, GUI.camera);
    //THREEx.FullScreen.bindKey({
    //    charCode: 'f'.charCodeAt(0)
    //});

    GUI.controls = new THREE.OrbitControls(GUI.camera, GUI.renderer.domElement);
    GUI.controls.center.set(GUI.step * GUI.maxFloorX / 2, 100, GUI.step * GUI.maxFloorY / 2);
    GUI.controls.zoomOut(1.2);

    var skyBoxGeometry = new THREE.CubeGeometry(10000, 10000, 10000);
    var skyBoxMaterial = new THREE.MeshBasicMaterial({
        color: 0x9999ff,
        side: THREE.BackSide
    });

    GUI.skyBox = new THREE.Mesh(skyBoxGeometry, skyBoxMaterial);
    GUI.scene.add(GUI.skyBox);

    GUI.INTERSECTED = null;
    GUI.projector = new THREE.Projector();
    document.addEventListener('mousemove', GUI.onDocumentMouseMove, false);
    
    var clickStart, clickFlag = false;
    $(document)
        .on("mousedown", function(){
            clickStart = new Date().getTime();
            clickFlag = true;
        })
        .on("mousemove", function(){
            clickFlag = false;
        })
        .on("mouseup", function(e){
            var dif = new Date().getTime() - clickStart;
            
            
            if(clickFlag == true || (clickFlag == false && dif < 150)) {
                GUI.onClick(e);
            }
        });
   
    
    var positionY = 250;
    GUI.lights = [];
    
    positions = [
        [1, 1],
        [GUI.maxFloorX, 1],
        [1, GUI.maxFloorY],
        [GUI.maxFloorX, GUI.maxFloorY]
    ];
    
    for(var i = 0; i < 4; i++) {
        GUI.lights[i] = new THREE.PointLight(0xffffff);
        
        position = positions[i];
        
        GUI.lights[i].intensity = 0.4;
        GUI.lights[i].position.set(position[0] * GUI.step, positionY, position[1] * GUI.step);
        GUI.scene.add(GUI.lights[i]);
    }
    

    GUI.initFloor(GUI.maxFloorX, GUI.maxFloorY);
    GUI.neo.loadMesh(function () {
        GUI.neo.setPosition(GUI.neo.posX, GUI.neo.posY);
    });

};

GUI.onClick = function (event) {
    if(GUI.INTERSECTED != null || GUI.INTERSECTED != undefined) {
        var posX = GUI.INTERSECTED.position.x / GUI.step;
        var posY = GUI.INTERSECTED.position.z / GUI.step;
        
        var me = CLIENT.clients[CLIENT.id];
        
        if(me.targeted == true && me.target["x"] == posX && me.target["y"] == posY) {
            CLIENT.setTarget("-1", "-1");
        }else{
            CLIENT.setTarget(posX, posY);
        }
    }
};

GUI.onDocumentMouseMove = function (event) {
    GUI.mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    GUI.mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
};

GUI.initFloor = function (width, height) {

    var squareGeometry = new THREE.Geometry();
    squareGeometry.vertices.push(new THREE.Vector3(-1.0, 1.0, 0.0));
    squareGeometry.vertices.push(new THREE.Vector3(1.0, 1.0, 0.0));
    squareGeometry.vertices.push(new THREE.Vector3(1.0, -1.0, 0.0));
    squareGeometry.vertices.push(new THREE.Vector3(-1.0, -1.0, 0.0));
    squareGeometry.faces.push(new THREE.Face3(0, 1, 2));
    squareGeometry.faces.push(new THREE.Face3(0, 3, 2));

    GUI.floorColors = {
        0: 0x8d6e63,
        1: 0x795548
    };

    GUI.floor = [];
    for (i = 0; i <= width; i++) {
        GUI.floor[i] = [];
        for (j = 0; j <= height; j++) {
            var mesh = new THREE.Mesh(squareGeometry,
                new THREE.MeshBasicMaterial({
                    color: GUI.floorColors[(i + j) % 2],
                    side: THREE.DoubleSide
                }));
            mesh.scale.set(GUI.step / 2, GUI.step / 2, GUI.step / 2);
            mesh.position.set((i * GUI.step), -100, (j * GUI.step));
            mesh.rotation.x = Math.PI / 2;
            mesh.name = "floor";

            GUI.floor[i][j] = mesh;
            GUI.scene.add(GUI.floor[i][j]);
        }
    }
};

GUI.clearFloorOpacity = function () {
    for (var i = 0; i < GUI.maxFloorX; i++) {
        for (var j = 0; j < GUI.maxFloorY; j++) {
            GUI.setTargetOpacity(i, j, 1);
        }
    }
};

GUI.setFloorColor = function (x, y, color) {
    if (color === undefined) {
        color = GUI.floorColors[(x + y) % 2];
        
        //0x3e2723
        if((x == GUI.neo.posX - 2 || x == GUI.neo.posX + 2) && y == GUI.neo.posY) {
            color = 0x3e2723;
        }else if(x >= GUI.neo.posX - 1 && x <= GUI.neo.posX + 1 && y >= GUI.neo.posY - 1 && y <= GUI.neo.posY + 1) {
            color = 0x3e2723;
        }
    }

    if (GUI.floor[x] !== undefined && GUI.floor[x][y] !== undefined) {
        GUI.floor[x][y].material.color = new THREE.Color(color);
        
        if(GUI.INTERSECTED && GUI.INTERSECTED == GUI.floor[x][y])
            GUI.INTERSECTED = null;
    }
};

GUI.setFloorColorHex = function(x, y, color) {
    if (GUI.floor[x] !== undefined && GUI.floor[x][y] !== undefined) {
        GUI.floor[x][y].material.color.setStyle(color);
        
        if(GUI.INTERSECTED && GUI.INTERSECTED == GUI.floor[x][y])
            GUI.INTERSECTED = null;
    }
}

GUI.setTargetOpacity = function (x, y, value) {
    GUI.floor[x][y].material.opacity = value;
};

GUI.shootTennis = function (id, newX, newY) {
    var position = {
        x: GUI.balls[id].position.x,
        z: GUI.balls[id].position.z
    };
    
    var target = {
        x: newX * GUI.step,
        z: newY * GUI.step
    };

    var time = 1000;
    var tween = new TWEEN.Tween(position).to(target, time);

    if (newX === GUI.neo.posX && newY === GUI.neo.posY) {
            
        setTimeout(function () {
            var audio = new Audio('sound.mp3');
            audio.play();

        }, time - 200);
    }
    
    tween.easing(TWEEN.Easing.Quadratic.In);
    tween.onUpdate(function () {
        GUI.balls[id].position.x = position.x;
        GUI.balls[id].position.z = position.z;
    });

    tween.start();
};

GUI.setFloorNearNeo = function() {
    x = GUI.neo.posX;
    y = GUI.neo.posY;
    for (var i = 0; i < GUI.floor.length; i++) {
            for (var j = 0; j < GUI.floor.length; j++) {
            if(GUI.notTargeted(i,j))    
                GUI.setFloorColor(i, j);
            }
        }

    for (var i = x - 1; i <= x + 1; i++)
        for (var j = y - 1; j <= y + 1; j++)
            if(GUI.notTargeted(i, j))
                GUI.setFloorColor(i, j, 0x3e2723);

    for (var i = x - 2; i <= x + 2; i++)
        if(GUI.notTargeted(i, j))
            GUI.setFloorColor(i, y, 0x3e2723);
};

GUI.neo = {
    posX: 0,
    posY: 0,

    loadMesh: function (callback) {

        var jsonLoader = new THREE.JSONLoader();
        jsonLoader.load("models/Neo.js", GUI.neo.addToScene);

        GUI.neo.cb = callback;
    },

    addToScene: function (geometry, materials) {
        var material = new THREE.MeshFaceMaterial(materials);
        for(var i in material.materials) {
            material.materials[i].color.setHex(0xFFFFFF);
        }
        
        GUI.neo.mesh = new THREE.Mesh(geometry, material);
        GUI.neo.mesh.name = "Neo";

        GUI.neo.mesh.scale.set(GUI.step, GUI.step, GUI.step);
        GUI.neo.mesh.position.x = 0;
        GUI.neo.mesh.position.y = -100;
        GUI.neo.mesh.position.z = 0;
        GUI.scene.add(GUI.neo.mesh);

        GUI.neo.cb();
    },

    setPosition: function (x, y, animated) { 
        if(animated == undefined) {
            animated = false;
        }
        
        if(animated) {
            var position = { x : GUI.neo.mesh.position.x, z: GUI.neo.mesh.position.z };
            var target = { x : x * GUI.step, z: y * GUI.step };
            var tween = new TWEEN.Tween(position).to(target, 500);
            tween.onUpdate(function(){
                GUI.neo.mesh.position.x = position.x;
                GUI.neo.mesh.position.z = position.z;
            });
            tween.onComplete(GUI.setFloorNearNeo);
            tween.easing(TWEEN.Easing.Quintic.InOut)
            tween.start();
        }else{

            GUI.neo.mesh.position.x = x * GUI.step;
            GUI.neo.mesh.position.z = y * GUI.step;
            GUI.setFloorNearNeo();
        }

        GUI.neo.posX = x;
        GUI.neo.posY = y;
    }
};

GUI.animate = function () {
    requestAnimationFrame(GUI.animate);
    GUI.render();
    GUI.update();
};

GUI.hoverCheck = function () {
    // find intersections
    // function by threejs!

    try {
        var vector = new THREE.Vector3(GUI.mouse.x, GUI.mouse.y, 1);
        GUI.projector.unprojectVector(vector, GUI.camera);
        var ray = new THREE.Raycaster(GUI.camera.position, vector.sub(GUI.camera.position).normalize());

        var intersects = ray.intersectObjects(GUI.scene.children);
        
        var inters = [];
        for(var i in intersects) {
            if(intersects[i].object.name == "floor") {
               inters[inters.length] = intersects[i];
            }
        }
        
        if (inters.length > 0) {
            if (inters[0] !== GUI.INTERSECTED) {
                // restore previous intersection object (if it exists) to its original color
                if (GUI.INTERSECTED)
                    GUI.INTERSECTED.material.color.setHex(GUI.INTERSECTED.currentHex);
                // store reference to closest object as current intersection object
                GUI.INTERSECTED = inters[0].object;
                // store color of closest object (for later restoration)
                GUI.INTERSECTED.currentHex = GUI.INTERSECTED.material.color.getHex();
                // set a new color for closest object
                GUI.INTERSECTED.material.color.setHex(0x333333);
            }
        } else // there are no intersections
        {
            // restore previous intersection object (if it exists) to its original color
            if (GUI.INTERSECTED)
                GUI.INTERSECTED.material.color.setHex(GUI.INTERSECTED.currentHex);
            // remove previous intersection object reference
            //     by setting current intersection object to "nothing"
            GUI.INTERSECTED = null;
        }
    } catch (error) {
        GUI.INTERSECTED = null;
    }
};

GUI.shoot = function() {
    
    for(var id in CLIENT.clients) {
        person = CLIENT.clients[id];
        ball = GUI.balls[id];
        
        GUI.shootTennis(id, person.target["x"], person.target["y"]);
    }
};

GUI.shootTennis = function (id, newX, newY) {
    var position = {
        x: GUI.balls[id].position.x,
        y: GUI.balls[id].position.y,
        z: GUI.balls[id].position.z
    };
    
    var target = {
        x: newX * GUI.step,
        y: -50,
        z: newY * GUI.step
    };

    var time = 1000;
    var tween = new TWEEN.Tween(position).to(target, time);

    if (newX === GUI.neo.posX && newY === GUI.neo.posY) {
            
        setTimeout(function () {
            var audio = new Audio('sound.mp3');
            audio.play();

        }, time - 200);
    }
    
    
    tween.easing(TWEEN.Easing.Quadratic.In);
    tween.onUpdate(function () {
        GUI.balls[id].position.x = position.x;
        GUI.balls[id].position.y = position.y;
        GUI.balls[id].position.z = position.z;
    });

    tween.start();
};


GUI.createTennis = function (id) {
    var mesh = THREEx.SportBalls.createTennis(CLIENT.clients[id].color);

    var newId = id + 1;
    var Xextra = Math.ceil(newId / 5);
    if (Xextra % 2 === 0) {
        Xextra *= -1;
    }

    var posX = GUI.step * CLIENT.clients[id].posX;
    var posY = -50;
    var posZ = GUI.step * CLIENT.clients[id].posY;

    mesh.position.set(posX, posY, posZ);

    mesh.scale.set(GUI.step / 3, GUI.step / 3, GUI.step / 3);

    GUI.scene.add(mesh);

    mesh.random = getRandomInt(1, 100) / 1000;

    GUI.balls[id] = mesh;
};

GUI.deleteTennis = function (id) {
    GUI.scene.remove(GUI.balls[id]);
};

GUI.update = function () {
    var delta = GUI.clock.getDelta();
    
    GUI.hoverCheck();
    GUI.onRenderFunctions.forEach(function (onRenderFct) {
        onRenderFct(0.05, 1);
    });

    for (var i in GUI.balls) {
        var r = parseFloat(GUI.balls[i].random);
        GUI.balls[i].rotation.x += r;
        GUI.balls[i].rotation.y -= r;
    }

    TWEEN.update();

    GUI.controls.update();
    GUI.stats.update();
};

GUI.render = function () {
    GUI.renderer.render(GUI.scene, GUI.camera);
};


///TENNIS BALL
var THREEx	= THREEx	|| {};
THREEx.SportBalls	= {};
THREEx.SportBalls.baseURL	= '/';

THREEx.SportBalls.createTennis	= function(color){
	var baseURL	= THREEx.SportBalls.baseURL;
	var textureColor= THREE.ImageUtils.loadTexture(baseURL + 'images/TennisBallColor.png');
	var textureBump	= THREE.ImageUtils.loadTexture(baseURL + 'images/TennisBallBump.jpg');
        var textureSpecular = THREE.ImageUtils.loadTexture(baseURL + 'images/TennisBallSpecular.png');
	var geometry	= new THREE.SphereGeometry(0.5, 32, 16);
	var material	= new THREE.MeshPhongMaterial({
                color: color,
		map	: textureColor,
		bumpMap	: textureBump,
		bumpScale: 0.5,
                specularMap: textureSpecular,
                specular: new THREE.Color('grey')
	});
	var mesh	= new THREE.Mesh( geometry, material );
	return mesh	
};


////WINDOW FOCUS
var window_focus;

$(window).focus(function() {
    GUI.arrangeBalls();
    window_focus = true;
}).blur(function() {
    window_focus = false;
});


/////////FUNCTIONS
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getSize(obj) {
    var size = 0,
        key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


var host = window.location.origin.replace("http://", "").split(":")[0];
var port = 9999;

CLIENT.createSocket(host, port);