import socket as SOCKET
import select as SELECT
import hashlib
import base64
import struct

class User():
    def __init__(self, socket, address, handshake = None):
        if handshake == None:
            handshake = False
            
        self.socket = socket
        self.address = address
        self.handshake = handshake
    
    def getId(self):
        return self.address[1]
        

class UsersList():
    def __init__(self):
        self.users = []
        self.connections = []
        pass
    
    def add(self, user):
        self.users.append(user)
        self.connections.append(user.socket)
    
    def remove(self, user):
        try:
            self.users.remove(user)
            self.connections.remove(user.socket)
        except:
            pass
        
    def removeBySocket(self, socket):
        try:
            socket.close()
            self.connections.remove(socket)
            user = self.getUserBySocket(socket)
            self.users.remove(user)
            return user
        except:
            pass
        
    def getSockets(self):
        return self.connections
    
    def getUserBySocket(self, socket):
        for user in self.users:
            if user.socket == socket:
                return user
        return None
    

class WebSocketServer():
    
    MAX_BUFFER = 4096
    DEFAULT_PORT = 9999
    DEFAULT_HOST = "localhost"
    HANDSHAKE = ("HTTP/1.1 101 Switching Protocols\r\n"
                "Upgrade: websocket\r\n"
                "Connection: Upgrade\r\n"
                "Sec-WebSocket-Accept: $acceptKey\r\n"
                "Sec-WebSocket-Protocol: game\r\n\r\n")
    GUID = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
    
    def __init__(self, host = None, port = None):
        
        if host == None:
            host = WebSocketServer.DEFAULT_HOST
        
        if port == None:
            port = WebSocketServer.DEFAULT_PORT
            
        print host, port
            
        self.host = host
        self.port = port
            
        self.users = UsersList()
        self.server = SOCKET.socket(SOCKET.AF_INET, SOCKET.SOCK_STREAM)
        
        self.onInit()
        
        pass

    def listen(self):
       
        self.server.setsockopt(SOCKET.SOL_SOCKET, SOCKET.SO_REUSEADDR, 1)
        self.server.bind((self.host, self.port))
        self.server.listen(5)
        
        print "Server start listening..."
        
        while 1:
            try:
                readSockets, writeSockets, errorSockets = SELECT.select([self.server] + self.users.getSockets(), [], [])
                for socket in readSockets:
                    #conexiune noua
                    if socket == self.server:
                        socketObject, address = self.server.accept()
                        self.users.add(User(socketObject, address, False))
                    else:
                        self.read(socket)


            except select.error, e:
                print "Select error."
            except:
                print "Another error"

            if not (readSockets):
                print "Timeout error."
                
        #close after while
        self.server.close()
    
    def read(self, socket):
        #citesc string de lugime maxima MAX_BUFFER pana cand pot
        data = ""
        length = WebSocketServer.MAX_BUFFER
        while length == WebSocketServer.MAX_BUFFER:
            recv = socket.recv(WebSocketServer.MAX_BUFFER)
            length = len(recv)
            data = data + recv
        
        if data == "":
            self.onCloseConnection(self.users.removeBySocket(socket))
        else:
            user = self.users.getUserBySocket(socket)
            if user.handshake == False:
                self.doHandshake(data, user)
            else:
                decoded = self.decodeMessage(data)
                
                if decoded == False:
                    self.onCloseConnection(self.users.removeBySocket(socket))
                else:
                    self.onMessageReceive(user, decoded)
                
    def doHandshake(self, data, user):
        
        split = data.split("Sec-WebSocket-Key: ")
        key = split[1].splitlines()
        key = key[0]
        
        hashed = base64.b64encode(hashlib.sha1(key + WebSocketServer.GUID).digest())
        user.handshake = True
        send = WebSocketServer.HANDSHAKE.replace("$acceptKey", hashed)
        user.socket.send(send)
        self.onNewConnection(user)
 
    def decodeMessage(self, data):
        #debug: primim mesajul
        #print ":".join("{:02x}".format(ord(c)) for c in data)

        #ord(data[0:1]) - este 136 pentru iesire, este 129 pentru mesaj text
        #verificam codul
        code = (ord(data[0:1]) & 127)
        
        #print code
        
        #code 8 = iesire, 136 & 127 = 8
        if code == 8:
            return False
        
        #verificam lungimea
        #print ord(data[1:2])
        length = ord(data[1:2]) & 127
        
        start = 2
        if length == 126:
            # > means big idian
            # H means unsigned short int
            length = struct.unpack(">H", data[2:4])[0]
            start = 4
        elif length == 127:
            # Q means unsgined long long int
            length = struct.unpack(">Q", data[2:10])[0]
            start = 10
            
        #scoatem mastile
        masks = []
        for i in range(0, 4):
            masks.append(ord(data[start + i]))
            
        #decodam mesaju
        decoded = ""
        for i in range(start+4, length+start+4):
            decoded += chr(ord(data[i]) ^ masks[len(decoded) % 4])
            
        return decoded
 
    def encodeMessage(self, message):
        ret = ""
        ret = ret + chr(129)
        length = len(message)
        if length <= 125:
            ret = ret + chr(length)
        elif length >= 126 and length <= 65535:
            ret = ret + chr(126)
            ret = ret + struct.pack(">H", length)
        else:
            ret = ret + chr(127)
            ret = ret + struct.pack(">Q", length)
        ret = ret + message
        return ret
    
    def sendMessage(self, message, sockets = None):
        
        if sockets == None:
            sockets = self.users.getSockets()
        elif type(sockets).__name__ != 'list':
            sockets = [sockets]
            
        for socket in sockets:
            socket.send(self.encodeMessage(message))
    
    def onCloseConnection(self, user):
        pass
    
    def onNewConnection(self, user):
        pass
            
    def onInit(self):
        pass
 
    def onMessageReceive(self, data):
        pass