from WebSocketServer import WebSocketServer
import json
from random import randint
import random
import sys
import datetime

class Person():
    def __init__(self, x, y):
        self.posX = x
        self.posY = y
        self.name = ""
        self.color = '#{:06x}'.format(random.randint(0, 0xffffff))
        self.target = {"x":"-1", "y":"-1"}
        self.targeted = False
        self.score = 0
        
    def setTargetNull(self):
        self.target = {"x":"-1", "y":"-1"}
        self.targeted = False
    
    def setTarget(self, x, y):
        self.target = {"x": x, "y": y}
        self.targeted = True
    
    
    def setName(self, name):
        self.name = name;

class Neo():
    def __init__(self, x, y):
        self.posX = x
        self.posY = y
        
    def setPosition(self, x, y):
        self.posX = x;
        self.posY = y;

class PingPong(WebSocketServer):
    
    MIN_X = 0
    MIN_Y = 0
    MAX_X = 13
    MAX_Y = 11
    PERSON_DIST = 2
    
    def __init__(self, host = None, port = None):
        
        if host == None:
            host = WebSocketServer.DEFAULT_HOST
        
        if port == None:
            port = WebSocketServer.DEFAULT_PORT
            
        WebSocketServer.__init__(self, host, port)
        
        self.neo = Neo(randint(PingPong.MIN_X + 2, PingPong.MAX_X - 2), randint(PingPong.MIN_Y + 1, PingPong.MAX_Y - 1))
        self.people = {}
        
        
    def onNewConnection(self, user):
        print "A intrat " + str(user.getId())
        
        position = self.randomPersonPosition()
        self.people[user.getId()] = Person(position["posX"], position["posY"])
        
        self.sendMessage(json.dumps({"action": "init", "id": user.getId(),
                                    "minX": PingPong.MIN_X, "maxX": PingPong.MAX_X,
                                    "minY": PingPong.MIN_Y, "maxY": PingPong.MAX_Y,
                                    "personDist": PingPong.PERSON_DIST,
                                    "neoX": self.neo.posX, "neoY": self.neo.posY}), user.socket)
        
        for person in self.people:
            if person != user.getId():
                self.sendMessage(json.dumps({"action": "new", "id": person, 
                                             "name": self.people[person].name, "color": self.people[person].color,
                                             "posX": self.people[person].posX, "posY": self.people[person].posY,
                                             "score": self.people[person].score }), user.socket)


    
    def onCloseConnection(self, user):
        now = datetime.datetime.now()
        print now.strftime("%Y-%m-%d %H:%M") + ": a iesit " + str(user.getId())
        
        self.people.pop(user.getId())
        
        self.sendMessage(json.dumps({"action": "remove", "id": user.getId()}))
    
    def sendNeoPosition(self, sockets = None):
        self.sendMessage(json.dumps({"action": "neo", "posX": self.neo.posX, "posY": self.neo.posY}), sockets)
        
        
    def onMessageReceive(self, user, message):
        try:
             
            now = datetime.datetime.now()
            data = json.loads(message)
            
            print now.strftime("%Y-%m-%d %H:%M") + ": action " + str(data["action"]) + " de la " + str(user.getId())
            
            #send current users
            if data["action"] == "name":
                print "Name:", data["name"]
                self.people[user.getId()].setName(data["name"])
                self.sendMessage(json.dumps({"action": "new", "id": user.getId(), 
                                             "name": data["name"], "posX": self.people[user.getId()].posX, "posY": self.people[user.getId()].posY,
                                             "score": self.people[user.getId()].score,
                                             "color": self.people[user.getId()].color}))
            elif data["action"] == "message":
                print "Message:", data["message"]
                self.sendMessage(json.dumps({"action": "message", "id": user.getId(), "message": data["message"]}))
            elif data["action"] == "target":
                
                if data["posX"] == "-1":
                    self.people[user.getId()].setTargetNull()
                else:
                    self.people[user.getId()].setTarget(data["posX"], data["posY"])
                    
                self.sendMessage(json.dumps({"action": "target", "id": user.getId(), 
                                             "posX": self.people[user.getId()].target["x"],
                                             "posY": self.people[user.getId()].target["y"]}))
                
                if self.allTarget() == True:
                    self.moveNeo()
                    for person in self.people:
                        self.people[person].setTargetNull()
                
        except Exception as e:
            print e
            
    def randomPersonPosition(self):
        y = [PingPong.MIN_Y - PingPong.PERSON_DIST, PingPong.MAX_Y + PingPong.PERSON_DIST - 1]
        x = [PingPong.MIN_X - PingPong.PERSON_DIST, PingPong.MAX_X + PingPong.PERSON_DIST - 1]
        
        if randint(0, 1) == 1:
            posX = randint(x[0], x[1])
            posY = y[randint(0, 1)]
        else:
            posX = x[randint(0, 1)]
            posY = randint(y[0], y[1])
            
        return {"posX": posX, "posY": posY}
    
    def moveNeo(self):
        addY = randint(-1, 1);
        
        if addY == 0:
            addX = randint(-2, 2)
        else:
            addX = randint(-1, 1);
            
        posX = self.neo.posX + addX
        posY = self.neo.posY + addY
        
        if posX > PingPong.MAX_X:
            posX = PingPong.MAX_X - 1
        elif posX < PingPong.MIN_X:
            posX = 1
            
        if posY > PingPong.MAX_Y:
            posY = PingPong.MAX_Y - 1
        elif posY < PingPong.MIN_Y:
            if addX == -2 or addX == 2:
                posY = 0
            else:
                posY = 1
                
        for person in self.people:
            if self.people[person].target["x"] == posX and self.people[person].target["y"] == posY:
                self.people[person].score += 1
                self.sendMessage(json.dumps({"action":"score", "id": person, "score": self.people[person].score}))
                
        self.neo.setPosition(posX, posY)
        self.sendNeoPosition()
        
    def targetedCount(self):
        count = 0
        for person in self.people:
            if self.people[person].targeted == True:
                count = count + 1
        return count
                
    def allTarget(self):
        return self.targetedCount() == len(self.people)
        
        

if __name__ == "__main__":
    
    host = None
    port = None
    
    if len(sys.argv) > 1:
        host = str(sys.argv[1])
        port = int(sys.argv[2])
    
    server = PingPong(host, port)
    server.listen()
    